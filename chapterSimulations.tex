\chapter{Gyrokinetic simulations}
\label{ch:simulations}


\vspace{-\bigskipamount}
\section{GENE}
\label{sec:sim_GENE}
The GENE code~\cite{GENE, Jenko2001, Dannert2005a, Merz2008a} is a massively parallel gyrokinetic Vlasov code, solving the nonlinear time evolution of the gyrokinetic distribution functions on a fixed grid in phase space. 
The gyrokinetic equations are derived from the kinetic equations by performing an average of the particles' gyrations around the field lines, so that the equations follow the centre of gyration, rather than the explicit orbits.
This reduces the velocity space coordinates from three to two directions: parallel velocity and magnetic moment.
Following the conventions of GENE, these are represented by to as $v$ and $\mu$ respectively.
In real space, the radial ($x$) and bi-normal ($y$) dependencies are treated spectrally, i.e. those directions are discretised explicitly in $k$-space, whereas the toroidal ($z$) direction is discretised in real space.
Because all phase space coordinates are coupled nonlinearly, the decrease from six to five phase space coordinates means a significant increase in computational efficiency.

There are some requirements that need to be fulfilled, for this simplification of the equations to be appropriate.
First of all, the Larmor gyro-radii ($\rho$) of the plasma species have to be small, and the associated cyclotron frequencies ($\Omega$) large, compared to the system size ($\sim R$) and frequency ($\omega$) of the turbulent fluctuations respectively;
secondly, the fast motion of the particles along the field lines lead to the requirement that the typical wave length of the parallel structure of the turbulence ($1/k_\parallel$) is much longer than the perpendicular ditto ($1/k_\perp$);
and third, the  energy associated with turbulent fluctuations need to be small compared to the thermal background energy. 
This is called the gyrokinetic ordering, which generally holds for tokamak plasmas~\cite{Frieman1982, Jenko2001}.
Formally, this can be written:
\begin{equation}
 \label{eq:GKO}
 \frac{\rho}{R}\sim
 \frac{\omega}{\Omega}\sim
 \frac{k_\parallel}{k_\perp}\sim
 \frac{q\phi}{T}\ll 1.
\end{equation}

\section{Experiments in silico}
\label{sec:sim_in_silico}
In this work, GENE simulations were performed in a flux tube geometry with periodic boundary conditions in the perpendicular directions.
The flux tube is in essence a box that is elongated and twisted along with the $\vec{B}$ field as the field lines traverse the tokamak.
Its application relies on the assumption that the scales of the phenomena of interest are all small compared to the size of the flux tube.
The periodic boundary conditions also imply the assumption, that local effects dominate over global.
This is generally true in the core of the plasma. 

The instantaneous memory usage of a nonlinear GENE simulation is often of the order of several gigabyte.
This means that even conservatively saving run-time data is unfeasible.
Instead, GENE reduces the raw field data to physically comprehensible fields, which are saved to disk at intervals specified by the user.
An example of this is shown in figure~\ref{fig:phi-torus}, where a cross section of the simulation domain is shown.
The highlighted area corresponds to a cross section of the flux tube, whereas the rest of the annulus is approximated from the whole three dimensional data set.
The quantity shown is the fluctuations in the electrostatic potential $\phi$ near the end of a TE~mode simulation.
The saved data can be loaded into e.g. GENE's native diagnostics tool, and after further refinement, data for specific physical quantities can be extracted.

Images such as the one presented in figure~\ref{fig:phi-torus} are useful for providing a quick ``sanity checks'' for the simulations:
\begin{itemize}
 \item Are the turbulent features sufficiently small, compared to the domain size? 
 \item Are they large enough, compared to the resolution? 
 \item Are there features that look artificial?
\end{itemize}
Beyond that, however, the derived data is still difficult to compare directly with experiments -- numerical and physical alike -- before it has been further distilled.
By performing different averages over the simulation domain, scalar quantities are derived, such as mean fluctuation levels of particle densities and of the electrostatic potential, and integrated particle and heat fluxes across the flux tube boundaries.
Since such scalar quantities are often what is needed for further analysis, GENE by default calculates and saves a number of such averages at regular intervals.
Because they are scalars rather than fields, the resulting time series can afford a very good temporal resolution, without hampering simulation performance or running out of disk space.
Two such time series are presented in figure~\ref{fig:timetrace}.
They show the space averaged fluctuations in background ion density~($n_H^2$) and impurity flux~($\Gamma_Z$) for the same simulation as in figure~\ref{fig:phi-torus}.

\begin{figure}[t] %fig:phi-torus
  \centering
  \includegraphics[width=\figwidth]{figures/phi-torus}
  \caption[A poloidal cross section of the plasma]{A cut from the toroidal annulus made up of the flux tube as it twists around the torus following the $\vec{B}$ field; see figure~\ref{fig:configuration}.
 Shown are the fluctuations in the electrostatic potential ($\phi$).
 A cross-section of the flux tube with the side $\sim 125\,\rho$ is indicated.
 Data from NL~GENE simulation of TE~mode turbulence at $t\approx300\,R/c_s$; parameters as in figure~\ref{fig:Z_TEM}.}
  \label{fig:phi-torus}
\end{figure}
\begin{figure}[t] % fig:timetrace
 \centering
 \includegraphics[width=0.77\linewidth]{figures/timetrace}
 \caption[Time series of average fluxes and fluctuations]{Time series showing fluctuations in the main ion density ($n_H^2$) and impurity flux ($\Gamma_Z$) after averaging over the whole flux tube; see figure~\ref{fig:phi-torus}.
 The average impurity flux ($\left<\Gamma_Z\right>$) is calculated from $\Gamma_Z$, discarding the first portion so as not to include the linear phase of the simulation.
 $\left<\Gamma_Z\right>$ is used for finding the peaking factor for the impurity species; see figure~\ref{fig:Gamma}.
 NL~GENE simulation with He~impurities; parameters as in figure~\ref{fig:Z_TEM}, with $R/L_{n_Z}=1.5$.
 An estimated uncertainty of one standard error is indicated.}
 \label{fig:timetrace}
%\end{figure}
%\begin{figure}[t] % fig:Gamma 
% \centering
 \includegraphics[width=0.77\linewidth]{figures/Gamma}
 \caption[Impurity flux as a function of density gradient]{Impurity flux $(\Gamma_Z)$ dependence on the impurity density gradient ($R/L_{n_Z}$), illustrating the peaking factor ($PF$), the diffusivity ($D_Z$) and pinch ($V_Z$), and the validity of the linearity assumption of equation~\eqref{eq:transport_short} for trace impurities. 
 $\Gamma_Z$ is acquired as a time average of the impurity flux; see figure~\ref{fig:timetrace}.
 $D_Z$ and $V_Z$ are calculated from the data, taking the estimated uncertainty into account.
 NL~GENE simulations with He impurities; parameters as in figure~\ref{fig:Z_TEM}. 
 An estimated uncertainty of one standard error is indicated.}
 \label{fig:Gamma}
\end{figure}

In order to reach the quantities of principal interest in this study, however, the data needs to be even further condensed.
First, a time average is performed on time series of the impurity flux in order to obtain a mean flux~($\left<\Gamma_Z\right>$), as illustrated in \ref{fig:timetrace}. 
This average is performed for simulations with at least three different values for the impurity density gradient ($-R\grad n_Z/n_Z=R/L_{n_Z}$).
As is illustrated in figure~\ref{fig:Gamma}, the linearised impurity flux equation~\eqref{eq:transport_short} is then fitted to the obtained average fluxes.
The quotient of the obtained diffusion coefficient~($D_Z$) and convective velocity~($V_Z$) then yields the peaking factor ($PF$), which quantifies the balance of diffusive and convective transport for the impurity species (see section~\ref{sec:PF} for details).

Finally, $PF$ is calculated for several different values of e.g. the impurity charge ($Z$) in order to obtain a scaling, which can be compared to experiments and other models.
Such a scaling is presented in figure~\ref{fig:Z_TEM}, where the sample followed in the figures mentioned previously has been highlighted~(\textcolor{red}{$\bigcirc$}).

In the process of generating a scaling such as the nonlinear scaling in figure~\ref{fig:Z_TEM}, the equivalent of several terabyte of instantaneous data is distilled into just a couple of floating point numbers -- a remarkable compression rate, to say the least.
GENE can also be run in quasilinear mode, a method that is considerably less demanding when it comes computer resources since the non-linear coupling between length scales is ignored~\cite{Dannert2005a, Dannert2005b, Merz2008a}.
The method is only used to study one mode at a time, and only for the particular length scale $k_\theta\rho_s$ of choice. 
If the length scale is chosen appropriately, however, the quasilinear simulation will capture the essential features of the dynamics, and it is useful for getting a qualitative understanding of the physical processes. 
As used in this work, it captures the contribution from the most unstable mode, not from any sub-dominant modes.
The methodology is the same as for the nonlinear simulations.

