# torusttest.py -- A short script for drawing toroidal devices in
# 3d using mayavi.
# Copyright (C) 2011 Andreas Skyman
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# A short manual of sorts:
# * for help installing, see
#    http://github.enthought.com/mayavi/mayavi/installation.html
#    (I recommend manual installation, using easy_install, for the simple
#    reasont that the Ubuntu reposistory version didn't want to talk to 
#    ipython. Make sure the listed requirements are met first,
#    they are all in teh Ubuntu repository!)
#* start ipython in threaded mode
#    $ ipythoin -wthread
#    (add the -pylab flag if you want to have play with  matplotlib too)
#* import this module using e.g.
#    import torustest as t
#* create a TokaMaker obejct by e.g.
#    T = t.TokaMaker(*arguments)
#    (arguments are optional; see __init__ for their definitions!)
#* draw a torus by calling
#    T.draw_torus(*arguments)
#    (arguments are optional)
#* add poloidal, toroidal or sheared lines and arrows using
#    T.draw_torus_line(*arguments)
#    (arguments are optional)
#* a small demo is created by calling
#    T.make_coordinates()
#* T.draw_arrow() and T.make_magnetic_geometry are works in progress.
#* for specific help see
#    help T.mlab.<function>
#  and
#    http://github.enthought.com/mayavi/mayavi/mlab.html

import numpy as np
from mayavi import mlab
tau = 2*np.pi

class TokaMaker():
    def __init__(self, Z_shape=[0.0, 1.0, 0.0], R_shape=[3.0, -1.0, 0.0], 
                 color=(1.0, 0.8, 0.9), opacity=1.0):
        self.R_shape = R_shape
        self.Z_shape = Z_shape
        self.R = R_shape[0]
        self.a = -R_shape[1]
        self.kappa = Z_shape[1]/self.a
        self.delta = R_shape[2]
        self.opacity = opacity
        self.color = color

    def get_Z(self, Theta, Zns=[0.0, 1.3, 0.13, 0.065]):
        """Expansion for Z following: doi:10.1088/0741-3335/42/5/305
        and doi:10.1063/1.863562"""

        Z = np.zeros(Theta.shape)
        for n, Zn in enumerate(Zns):
            Z += Zn*np.sin(n*Theta)
        return Z

    def get_R(self, Theta, Rns=[3.5, -1.0, 0.1, 0.05]):
        """Expansion for R following: doi:10.1088/0741-3335/42/5/305
        and doi:10.1063/1.863562"""

        R = np.zeros(Theta.shape)
        for n, Rn in enumerate(Rns):
            R += Rn*np.cos(n*Theta)
        return R

    def draw_torus_line(self, phi_0=0, phi_max=1.0, theta_0=0, theta_max=0,
                        q=1.0, epsilon=1.0,
                        arrow='cone', color=(1.,0.,0.), resolution=100,
                        line_width=3.0, opacity=1.0, fign=0, clearit=False):
        """Draw a line or an arrow on the torus, based on angular coordinates.
        Distance r from toroidal axis is set by epsilon = r/a.
        Use either q or explicit theta angles to get the desired helicity."""

        # Prepare figure for plotting:
        f = mlab.figure(fign, (1.,1.,1.), (0.,0.,0.))
        if clearit:
            mlab.clf()

        # Prepare data:
        phi = np.linspace(phi_0, phi_max, resolution)
        if q != 0:
            theta = phi/q + theta_0
            if theta_max != 0:
                print "Can't use both q and theta_max: using q."
        else:
            theta = np.linspace(theta_0, theta_max, resolution)

        
        eps_R_shape = self.R_shape[:]
        eps_R_shape[1] *= epsilon
        eps_Z_shape = self.Z_shape[:]
        eps_Z_shape[1] *= epsilon
        
        R_shape = self.get_R(theta, eps_R_shape)
        Z_shape = self.get_Z(theta, eps_Z_shape)
        x = R_shape*np.cos(phi)
        y = R_shape*np.sin(phi)
        z = Z_shape

        # Draw it:
        l = mlab.plot3d(x, y, z, line_width=line_width, opacity=opacity, 
                        color=color)
        if arrow:
            dx = x[-1] - x[-2]
            dy = y[-1] - y[-2]
            dz = z[-1] - z[-2]
            dr = np.sqrt(dx**2 + dy**2 + dz**2)
            q = mlab.quiver3d(x[-2], y[-2], z[-2], dx/dr, dy/dr, dz/dr, 
                              line_width=line_width, opacity=opacity, 
                              scale_factor=0.2*line_width, mode=arrow,
                              resolution=36, color=color)
            mlab.show()
            return l, q
        else:
            mlab.show()
            return l
                
    def draw_torus(self, Phi_0=0.0, Phi_max=tau*2/3, nPhi=50,
                   Theta_0=0.0, Theta_max=2.01*np.pi, nTheta=50, 
                   fign=0, clearit=True):
        """Draw a torus.""" 

        # Prepare figure for plotting:
        f = mlab.figure(fign, (1.,1.,1.), (0.,0.,0.))
        if clearit:
            mlab.clf()

        # Prepare torus-data:
        dTheta, dPhi = tau/nTheta, tau/nPhi
        [Theta, Phi] = np.mgrid[Theta_0 : Theta_max + dTheta : dTheta,
                                Phi_0   : Phi_max + dPhi     : dPhi]
        R_shape = self.get_R(Theta, self.R_shape)
        Z_shape = self.get_Z(Theta, self.Z_shape)
        x = R_shape*np.cos(Phi)
        y = R_shape*np.sin(Phi)
        z = Z_shape

        # Plot and show:
        s = mlab.mesh(x, y, z, opacity=self.opacity, color=self.color)
        mlab.show()

    def draw_arrow(self, phi=[0, 0], theta=[0, 0], epsilon=[3,0],
                   arrow='cone', color=(1.,0.,0.), resolution=100,
                   line_width=3.0, opacity=1.0, fign=0, clearit=False):
        """Draw an arroe (or line) in toroidal coordinates."""
        # Prepare figure for plotting:
        f = mlab.figure(fign, (1.,1.,1.), (0.,0.,0.))
        if clearit:
            mlab.clf()
        
        # Prepare data:
        x, y, z = [], [], []
        for i, e in enumerate(epsilon):
            eps_R_shape = self.R_shape[:]
            eps_R_shape[1] *= e
            eps_Z_shape = self.Z_shape[:]
            eps_Z_shape[1] *= e
        
            R_shape = self.get_R(np.array(theta[i]), eps_R_shape)
            Z_shape = self.get_Z(np.array(theta[i]), eps_Z_shape)
            x.append(R_shape*np.cos(np.array(phi[i])))
            y.append(R_shape*np.sin(np.array(phi[i])))
            z.append(Z_shape)

        # Draw it:
        l = mlab.plot3d(x, y, z, line_width=line_width, opacity=opacity, 
                        color=color)
        if arrow:
            dx = x[-1] - x[-2]
            dy = y[-1] - y[-2]
            dz = z[-1] - z[-2]
            dr = np.sqrt(dx**2 + dy**2 + dz**2)
            q = mlab.quiver3d(x[-1], y[-1], z[-1], dx/dr, dy/dr, dz/dr, 
                              line_width=line_width, opacity=opacity, 
                              scale_factor=0.2*line_width, mode=arrow,
                              resolution=36, color=color)
            mlab.show()
            return l, q
        else:
            mlab.show()
            return l

    def make_coordinates(self, fign=1):
        # Draw torus:
        self.draw_torus(fign=fign)

        # Draw angular coordinates:
        #mlab.text(self.R, 0, ' poloidal\ndirection', z=-self.a*2.0, width=0.2)
        self.draw_torus_line(phi_0=tau/180, phi_max=tau/180,
                             theta_0=tau,theta_max=tau*1/12, 
                             q=0.0, epsilon=-1.25, fign=fign, 
                             color=(1.0,0.5,0.0), line_width=2)
        #mlab.text(0, self.R, ' toroidal\ndirection', z=self.a*1.25, width=0.2)
        self.draw_torus_line(phi_0=-tau*1/360, phi_max=tau*20/21,
                             theta_0=tau/4, theta_max=tau/4,
                             q=0.0, epsilon=0.0, fign=fign, 
                             color=(1.0,0.0,0.0))

        # Draw radii:
        self.draw_arrow(phi=[0,0], theta=[0,0], epsilon=[3, 0.6], 
                        fign=fign, line_width=3, color=(0.1,0.1,0.1))
        self.draw_arrow(phi=[0,0], theta=[0,tau*3/8], epsilon=[0, 0.6], 
                        fign=fign, line_width=2, color=(0.0,0.0,1.0))

        # Set view and show:
        mlab.view(-90, 90-35.264)
        mlab.show()
    
    def make_magnetic_geometry(self, fign=1):
        # Draw torus:
        self.draw_torus(fign=fign)

        # Draw magnetic fields:
        ### Remains: Color and unicode and the rest
        # Poloidal component:
        #mlab.text(self.R, 0, 'poloidal', z=-self.a*2.0, width=0.2)
        self.draw_torus_line(phi_0=tau*1/12, phi_max=tau*1/12,
                             theta_0=tau, theta_max=1.2*tau*1/4,
                             q=0, epsilon=1.01, fign=fign,
                             line_width=1.5, color=(0.1, 0.05, 0.0))
        self.draw_torus_line(phi_0=tau*1/12, phi_max=tau*1/12,
                             theta_0=0.99*tau*1/4, theta_max=2.5*0.35*tau*1/12,
                             q=0, epsilon=1.01, fign=fign,
                             line_width=2, color=(1.0, 0.5, 0.0))

        # Toroidal component:
        #mlab.text(0, self.R, 'toroidal', z=self.a*1.25, width=0.2)
        self.draw_torus_line(phi_0=1.07*tau*1/12, phi_max=tau*2/3,
                             theta_0=tau/4, theta_max=tau/4,
                             q=0.0, epsilon=1.01, fign=fign,
                             line_width=1.5, color=(0.1, 0.0, 0.0))
        self.draw_torus_line(phi_0=0.0, phi_max=0.8*tau*1/12,
                             theta_0=tau/4, theta_max=tau/4,
                             q=0.0, epsilon=1.01, fign=fign,
                             line_width=2, color=(1.0, 0.0, 0.0))

        # Resulting field:
        #mlab.text(self.R, 0, '  total', z=-self.a*2.0, width=0.2)
        self.draw_torus_line(phi_0=1.05*tau*1/12, phi_max=tau*2/3,
                             theta_0=tau*1/4, theta_max=0,
                             q=-0.35, epsilon=1.01, fign=fign,
                             line_width=1.5, color=(0.0, 0.0, 0.1))
        self.draw_torus_line(phi_0=0, phi_max=0.8*tau*1/12,
                             theta_0=tau*1/4, theta_max=0,
                             q=-0.35, epsilon=1.01, fign=fign,
                             line_width=2, color=(0.0, 0.0, 1.0))

        # Draw plasma current:
        ### Remains: Color and text
        #mlab.text(0, -self.R, 'induced\ncurrent', z=self.a*1.25, width=0.2)
        self.draw_torus_line(phi_0=tau*1/3, phi_max=-tau*9/30,
                             theta_0=0.0, theta_max=0.0,
                             q=0.0, epsilon=0.0, fign=fign, 
                             color=(0.1, 0.9, 0.0), line_width=4)
        #mlab.text(0, -self.R, 'induced\n field', z=-self.a*2.0, width=0.2)
        self.draw_torus_line(phi_0=-tau*1/6, phi_max=-tau*1/6,
                             theta_max=tau*1/60, theta_0=tau*5/6,
                             q=0.0, epsilon=0.5, fign=fign,
                             color=(0.0, 0.1, 0.0), line_width=2)

        # Draw radii:
        self.draw_arrow(phi=[0,0], theta=[0,0], epsilon=[3, 0.6], 
                        fign=fign, line_width=3, color=(0.1,0.1,0.1))
        self.draw_arrow(phi=[0,0], theta=[0,tau*3/8], epsilon=[0, 0.6], 
                        fign=fign, line_width=2, color=(0.9,0.9,1.0))

        # Set view and show:
        mlab.view(-90, 90-35.264)
        mlab.show()

    def make_magnetic_geometry_2(self, fign=1):
        # Draw torus:
        self.draw_torus(fign=fign)

        # Draw magnetic fields:
        ### Remains: Color and unicode and the rest
        # Poloidal component:
        #mlab.text(self.R, 0, 'poloidal', z=-self.a*2.0, width=0.2)
        self.draw_torus_line(phi_0=tau*1/12, phi_max=tau*1/12,
                             theta_0=tau, theta_max=1.2*tau*1/4,
                             q=0, epsilon=1.01, fign=fign,
                             line_width=1.5, color=(0.1, 0.05, 0.0))
        self.draw_torus_line(phi_0=tau*1/12, phi_max=tau*1/12,
                             theta_0=0.99*tau*1/4, theta_max=2.5*0.35*tau*1/12,
                             q=0, epsilon=1.01, fign=fign,
                             line_width=2, color=(1.0, 0.5, 0.0))

        # Toroidal component:
        #mlab.text(0, self.R, 'toroidal', z=self.a*1.25, width=0.2)
        self.draw_torus_line(phi_0=1.07*tau*1/12, phi_max=tau*2/3,
                             theta_0=tau/4, theta_max=tau/4,
                             q=0.0, epsilon=1.01, fign=fign,
                             line_width=1.5, color=(0.1, 0.0, 0.0))
        self.draw_torus_line(phi_0=0.0, phi_max=0.8*tau*1/12,
                             theta_0=tau/4, theta_max=tau/4,
                             q=0.0, epsilon=1.01, fign=fign,
                             line_width=2, color=(1.0, 0.0, 0.0))

        # Resulting field:
        #mlab.text(self.R, 0, '  total', z=-self.a*2.0, width=0.2)
        self.draw_torus_line(phi_0=1.05*tau*1/12, phi_max=tau*2/3,
                             theta_0=tau*1/4, theta_max=0,
                             q=-0.35, epsilon=1.01, fign=fign,
                             line_width=1.5, color=(0.0, 0.0, 0.1))
        self.draw_torus_line(phi_0=0, phi_max=0.8*tau*1/12,
                             theta_0=tau*1/4, theta_max=0,
                             q=-0.35, epsilon=1.01, fign=fign,
                             line_width=2, color=(0.0, 0.0, 1.0))

        # Draw plasma current:
        ### Remains: Color and text
        #mlab.text(0, -self.R, 'induced\ncurrent', z=self.a*1.25, width=0.2)
        self.draw_torus_line(phi_0=tau*1/3, phi_max=-tau*3/30,
                             theta_0=0.0, theta_max=0.0,
                             q=0.0, epsilon=0.0, fign=fign, 
                             color=(0.1, 0.9, 0.0), line_width=4)
        #mlab.text(0, -self.R, 'induced\n field', z=-self.a*2.0, width=0.2)
        self.draw_torus_line(phi_0=-tau*1/30, phi_max=-tau*1/30,
                             theta_max=tau*1/60, theta_0=tau*5/6,
                             q=0.0, epsilon=0.5, fign=fign,
                             color=(0.0, 0.1, 0.0), line_width=2)

        # Set view and show:
        mlab.view(-90, 90-35.264)
        mlab.show()
