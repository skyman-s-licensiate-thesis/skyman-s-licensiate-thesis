%% Copyright (c) 2010, 2011: Markus Billeter                         -{{{2-
 %
 % Permission is hereby granted, free of charge, to any person obtaining a copy
 % of this software and associated documentation files (the "Software"), to deal
 % in the Software without restriction, including without limitation the rights
 % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 % copies of the Software, and to permit persons to whom the Software is
 % furnished to do so, subject to the following conditions:
 %
 % The above copyright notice and this permission notice shall be included in
 % all copies or substantial portions of the Software.
 %
 % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%% THE SOFTWARE.

% --{{{2- Page setup. The format at Chalmers is apparently G5 (169mm x 239mm).  
% Other values are inherited from the previous template.
\def\licPageWidth{169mm}
\def\licPageHeight{239mm}
\def\licContentWidth{122mm}
\def\licContentHeight{190mm}
\def\licMarginLeft{22.3mm}
\def\licMarginBottom{22mm}
\def\licMarginRight{\licPageWidth-\licContentWidth-\licMarginLeft}
\def\licMarginTop{\licPageHeight-\licContentHeight-\licMarginBottom}

\geometry{
	verbose,
	%nohead,
        headsep=\bigskipamount,
	papersize={\licPageWidth, \licPageHeight},
	bmargin=\licMarginBottom,
	lmargin=\licMarginLeft,
	rmargin=\licMarginRight,
	tmargin=\licMarginTop,
	width=\licContentWidth,
	height=\licContentHeight,
	driver=pdftex,
	%showframe % XXX-IMPORTANT: Comment this option in the final version
}

\titlespacing{\section}{0pt}{*0}{*0}
\titlespacing{\subsection}{0pt}{*0}{*0}
\titlespacing{\subsubsection}{0pt}{*0}{*0}

% --{{{2- Configure `hyperref' package
\definecolor{hyColor}{rgb}{0.1,0.3,0.4}
\definecolor{hySmallLink}{rgb}{0.2,0.6,0.8}
\hypersetup{
	unicode=true,
	pdftitle={\licTitle},
	pdfauthor={\licAuthorFirst \licAuthorLast},
	pdfsubject={},
	pdfcreator={},
	pdfproducer={},
	colorlinks=true,
	linkcolor=hyColor,
	citecolor=hySmallLink,
	urlcolor=hySmallLink,
	filecolor=hyColor
}

% --{{{2- Configure `pdfpages' package
\includepdfset{
	pages=-, % include all pages
}

% --{{{2- Configure `caption' package
\captionsetup{
	labelfont={small,bf,sf},
	textfont={small,it,sf}
}
\captionsetup[subfigure]{
	singlelinecheck=true,
	labelfont={small,sl,sf},
	textfont={small,sl,sf}
}

% --{{{2- Configuration: replacement tokens
\def\charTilde{{\raise.27ex\hbox{$\scriptstyle\sim$}}}
\def\charUnderscore{\mathunderscore} 

	% WARNING: don't use \_ for \charUnderscore, since that will cause weird 
	% errors in \StrSubstitute.

% --{{{2- Commands: various stuff
\newcommand{\inlineicon}[1]{\includegraphics[height=\baselineskip]{#1}}
\newcommand{\TODO}[1]{\todo[inline]{#1}}

\newcommand{\heading}[2]{
	\pdfbookmark[1]{#1}{#2}
	\section*{#1} \label{#2}
}

\newcommand{\resetPageCounter}[1]{
	\renewcommand{\thepage}{#1}
	\setcounter{page}{1}
}

\def\cleardoublepageempty{\clearpage{\pagestyle{empty}\cleardoublepage}}

% --{{{2- Commands: insert links
\newcommand{\webpage}[1]{\href{#1}{%
	\inlineicon{img/link-logo.png} \underline{%
		\StrSubstitute[0]{#1}{~}{\charTilde}[\mystr]%
		\StrSubstitute[0]{\mystr}{_}{\charUnderscore}}}%
}
\newcommand{\mailto}[1]{\href{mailto:#1}{%
	\inlineicon{img/email-logo.png} \underline{#1}} %
}

\newcommand{\pdflink}[1]{\href{#1}{%
	\inlineicon{img/pdf-logo.png} \underline{%
		\StrSubstitute[0]{#1}{~}{\charTilde}[\mystr]%
		\StrSubstitute[0]{\mystr}{_}{\charUnderscore}}}%
}
\newcommand{\doilink}[1]{\href{http://dx.doi.org/#1}{%
	\inlineicon{img/doi-logo.png} \underline{#1}} %
}
\newcommand{\youtube}[1]{\href{http://www.youtube.com/watch?v=#1}{%
	\inlineicon{img/film-logo.png} \underline{http://www.youtube.com/watch?v=#1}} %
}

% --{{{2-- Commands: Add a paper
\makeatletter
\def\contribPaper{\@ifnextchar[\@contribPaper{\@contribPaper[]}}
\def\@contribPaper[#1]<#2|#3>{{%
	\setkeys{contPapSet}{#1}%
	%
	% sanitize arguments (assign default values, etc)
	\ifx\contribPaper@pages\undefined%
		\def\contribPaper@pagesA{}%
		\def\contribPaper@pagesB{}%
	\else%
		\def\contribPaper@pagesA{\contribPaper@pages }
		\def\contribPaper@pagesB{\contribPaper@pages \\}
	\fi%
	\ifx\contribPaper@location\undefined%
		\def\contribPaper@locationA{}%
		\def\contribPaper@locationB{}%
	\else%
		\def\contribPaper@locationA{\contribPaper@location, }
		\def\contribPaper@locationB{\contribPaper@location \\}
	\fi%
	\ifx\contribPaper@journal\undefined%
		\def\contribPaper@journalA{}%
		\def\contribPaper@journalB{}%
	\else%
		\def\contribPaper@journalA{\contribPaper@journal, }
		\def\contribPaper@journalB{\contribPaper@journal \\}
	\fi%
	\ifx\contribPaper@inset\undefined%
		\def\contribPaper@insetB{}%
	\else%
		\def\contribPaper@insetB{\contribPaper@inset}
	\fi%
	%
	% title double-page with no page numbers
	\renewcommand{\thepage}{}%
	\cleardoublepage%
	\renewcommand{\thepage}{\arabic{page}}%
	\thispagestyle{empty}
	%
	% add to TOC and paper list
	\refstepcounter{paperid}%
	\addcontentsline{toc}{chapter}{%
		~~Paper \Roman{paperid} \textnormal{\small{-- \contribPaper@title}}%
	}
	\addcontentsline{ppr}{paper}{\protect{%
		\contribPaper@authors, \\
		\sffamily{\contribPaper@title}, \\
		\textit{\contribPaper@journalA \contribPaper@pagesA 
		\contribPaper@locationA}
	}}%
	%
	% labels
	\label{#2}\label{paper:\Roman{paperid}}%
	%
	% title page
	{\Large\bf\hfill\sffamily  Paper \Roman{paperid} }

	\begin{centering}
	\vspace*{30ex}

	{\Large\bf\sffamily \contribPaper@title }\\
	\vspace{0.2cm}
	{\sffamily \contribPaper@authors} \\
	\vspace{0.5cm}
	\contribPaper@insetB
	\contribPaper@journalB
	\contribPaper@pagesB
	\contribPaper@locationB
	\vspace{1.2cm}
	\ifx\contribPaper@thumb\undefined%
	\else %meh
		\includegraphics[width=0.8\linewidth]{\contribPaper@thumb}
	\fi%

	\end{centering}
	\ifx\contribPaper@footer\undefined%
	\else
	\vfill
	{\small{
		\contribPaper@footer
	}}
	\fi
	%
	% include PDF with special page numbers
	\clearpage{\pagestyle{empty}\cleardoublepage}
	\ifx\contribPaper@pdf\undefined
	\else
		\renewcommand{\thepage}{
			\arabic{page} \small{\sffamily(Paper 
				\Roman{paperid}:\arabic{paperPage})}
		}
		\includepdf[pagecommand={\stepcounter{paperPage}},#3]
			{\contribPaper@pdf}
		\renewcommand{\thepage}{\arabic{page}}
	\fi
}}

\define@key{contPapSet}{title}{\def\contribPaper@title{#1}}
\define@key{contPapSet}{authors}{\def\contribPaper@authors{#1}}
\define@key{contPapSet}{journal}{\def\contribPaper@journal{#1}}
\define@key{contPapSet}{location}{\def\contribPaper@location{#1}}
\define@key{contPapSet}{pages}{\def\contribPaper@pages{#1}}
\define@key{contPapSet}{thumb}{\def\contribPaper@thumb{#1}}
\define@key{contPapSet}{pdf}{\def\contribPaper@pdf{#1}}
\define@key{contPapSet}{footer}{\def\contribPaper@footer{#1}}
\define@key{contPapSet}{inset}{\def\contribPaper@inset{#1}}
\makeatother

	%
	% --{{{3- internal data
	\newcounter{paperid}
	\newcounter{paperPage}[paperid]
	\def\thepaperid{Paper~\textbf{\Roman{paperid}}}

% --{{{2-- Commands: List of Papers
\makeatletter
\newcommand{\listofpapers}{%
	\newcounter{PaperLCount}%
	\begin{list}{%
		\hyperref[paper:\Roman{PaperLCount}]{Paper \Roman{PaperLCount}} --
	}{\usecounter{PaperLCount}}%
	\@starttoc{ppr}%
	\end{list}%
}

	%
	% --{{{3- callback: called for each paper entry
	\newcommand{\l@paper}[2]{\item{#1}\\}
\makeatother

