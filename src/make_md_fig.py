import re as regex
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.mlab import find

BE = 'binding energy'
BEN = 'binding energy per nucleon'
UNITS = {}
UNITS['A'] = '$A\, \mathrm{(mass\, number)}$'
UNITS['Z'] = '$Z \mathrm{(proton number)}$'
UNITS['m'] = '$m\, \mathrm{(MeV}/c^2\mathrm{)}$'
UNITS[BE]  = '$B\, \mathrm{(MeV)}$'
UNITS[BEN] = '$B/A\, \mathrm{(MeV\, per\, nucleon})$'


class Data_Object():
    def __init__(self, filename='stable_data.txt'):
        self.filename = filename
        self.data = {}
        self.stable = {}

        self.raw_data = self.load_nucl_data(self.filename)
        self.rearrange_data(self.raw_data)
        self.load_stable()

        self.msize = 10
        self.lwidth = (np.sqrt(5) + 1)/2
        self.fontsize='xx-large'
        self.colour = 'k'


    def load_nucl_data(self, filename='nuclear_data.txt'):
    	D = np.genfromtxt(filename, dtype=None, comments='#')

        return D

    def rearrange_data(self, D):
        self.data['A'] = D['f0']
        self.data['Z'] = D['f1']
        self.data['name'] = D['f2'] 
        self.data['m'] = D['f3']
        self.data['binding energy'] = D['f4']
        self.data['binding energy per nucleon'] = D['f5']

    def plot_data(self, x_key='A', y_key=BEN, clear_figure=True, fig=1, options='', 
            HL=['2H', '56Fe', '238U'], markers=['^', 's', 'p'], colours=['r', 'g', 'y']):
        plt.figure(fig)
        if clear_figure:
            plt.clf()

        x_data = self.get_data(x_key, options)
        y_data = self.get_data(y_key, options)

        plt.plot(x_data[1:], y_data[1:], '-', c=self.colour, 
                 ms=self.msize, lw=self.lwidth, mew=self.lwidth)
        #plt.scatter(x_data, y_data)

        plt.xlabel(UNITS[x_key], fontsize=self.fontsize)
        plt.ylabel(UNITS[y_key], fontsize=self.fontsize)

        if HL:
            self.highlight_data(x_key, y_key, HL, fig, markers, colours)
            plt.legend(numpoints=1, loc=4)
        plt.axis([x_data[1]-1, x_data[-1]+1, 0, np.ceil(y_data.max())])
        plt.grid('on')

    def get_data(self, key, options=''):
        if options.lower() == 'stable':
            data = []
            for i, d in enumerate(self.data[key]):
                if self.data['Z'][i] in self.stable.keys() and \
                        self.data['A'][i] in self.stable[self.data['Z'][i]]:
                    data.append(d)
            return data
        else:
            return self.data[key]
            
    def highlight_data(self, x_key='A', y_key=BEN, elements=['2H'], fig=1, 
                       markers=['o'], colours=['r']):
        plt.figure(fig)
        for n, e in enumerate(elements):
            i = find(self.data['name'] == e)
            name = regex.findall('[a-z,A-Z].*', e)[0]
            number = regex.findall('.*[0-9]', e)[0]
            x = self.data[x_key][i]
            y = self.data[y_key][i]
            plt.plot(x, y, markers[n], mec=colours[n], mfc='None', ms=self.msize, mew=self.lwidth, \
                label='$^{' + number + '}$'+ name )

    def load_stable(self, filename='stable.txt'):
        with open(filename, 'r') as f:
            l = f.readline()
            while l:
                d = self.str_to_array(l)
                self.stable[d[0]] =d[1:]
                l = f.readline()
    
    def save_stable(self, filename='stable_data.txt'):
        with open(filename, 'w') as f:
            A = self.get_data('A', options='stable')
            Z = self.get_data('Z', options='stable')
            name = self.get_data('name', options='stable')
            m = self.get_data('m', options='stable')
            be = self.get_data(BE, options='stable')
            ben = self.get_data(BEN, options='stable')
            header = """\
# List of experimentally stable isotopes.
#
# The columns represent:
# 1. Mass number, A (number of nucleons) 
# 2. Atomic number, Z (number of protons)
# 3. Isotope name
# 4. Mass, M (MeV/c**2)
# 5. Binding energy, B (MeV)
# 6. Binding energy per nucleon, B/A (MeV per nucleon)
#
# Data from:
#     Markus Poessel, "Is the whole the sum of its parts?", Einstein Online Vol. 4, 1003 (2010)
#     Kenneth S. Krane, "Introductory Nuclear Physics". John Wiley & Sons, 1988
#
# Compiled by Andreas Skyman, 2011
#
# This file is Public Domain.
# For information on what this means, please refer to e.g.:
#     http://creativecommons.org/publicdomain/zero/1.0/
#
"""
            f.write(header)
            for i in xrange(len(A)):
                l = `A[i]` + " " + `Z[i]` + " " + name[i] + " " + `m[i]` + " " + `be[i]` + " " + `ben[i]` + "\n"
                f.write(l)

    def str_to_array(self, l):
        d = []
        for n in l.split(' '):
            d.append(int(n))
        return np.array(d)


def plot_mass_defect(filename='stable_data.txt', x_key='A', y_keys=['m', 'A'], clear_figure=True, fig=2, options='', 
        HL=['2H', '56Fe', '238U'], markers=['^', 's', 'p'], colours=['r', 'g', 'y']):

    D = Data_Object(filename)

    plt.figure(fig)
    if clear_figure:
        plt.clf()

    x_data = D.get_data(x_key, options)
    y_data = D.get_data(y_keys[0], options)/D.get_data(y_keys[1], options)

    plt.plot(x_data[:], y_data[:], '-', c=D.colour, 
        ms=D.msize, lw=D.lwidth, mew=D.lwidth)
    #plt.scatter(x_data, y_data)

    plt.xlabel(UNITS[x_key], fontsize=D.fontsize)
    plt.ylabel('$m/A\, \mathrm{(MeV}/c^2\, \mathrm{per\, nucleon)}$', fontsize=D.fontsize)

    if HL:
        for n, e in enumerate(HL):
            i = find(D.data['name'] == e)
            name = regex.findall('[a-z,A-Z].*', e)[0]
            number = regex.findall('.*[0-9]', e)[0]
            x = D.data[x_key][i]
            y = D.data[y_keys[0]][i]/D.data[y_keys[1]][i]
            plt.plot(x, y, markers[n], mec=colours[n], mfc='None', ms=D.msize, mew=D.lwidth, \
                label='$^{' + number + '}$'+ name )
        plt.legend(numpoints=1, loc=1)
    plt.axis([x_data[1]-1, x_data[-1]+1, np.floor(y_data.min()), np.ceil(y_data.max())])
    plt.axis([-3, 242, 930, 939])
    plt.grid('on')

