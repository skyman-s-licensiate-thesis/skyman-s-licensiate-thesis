\chapter{Introduction}
\label{ch:introduction}
%The topic of this thesis is precisely that which the title implies:
%\emph{turbulent impurity transport in tokamak fusion plasmas}. 
%This one-line summary -- consisting, as it does, almost exclusively of keywords -- is, however, most likely entirely unhelpful to non-experts. 
%The aim, therefore, of this introductory chapter is to make the non-expert reader sufficiently acquainted with the words used in the title, as to provide a context for the rest of the thesis. 
%Starting with a very short introduction to the broad field of fusion physics, the scope will be narrowed in each subsequent section, until the specific application of impurity transport is reached, where after the context of this particular thesis will be briefly expanded upon. 
%My hope is that this red thread will provide enough guidance, that also the non-expert reader will dare venture into the labyrinthine windings that follow the introduction.
%
%The remainder of the thesis is structured as follows: 
%after the introduction, chapter~\ref{ch:transport} will introduce in more detail the theory of impurity transport; 
%chapter~\ref{ch:simulations} will introduce the simulations through which data has been produced, as well as the main methods employed to analyse this data; 
%in chapter~\ref{ch:summary} the included papers are summarised in the light of the preceding chapters; 
%finally, chapter~\ref{ch:conclusion} provides a brief summary of the whole thesis, the main conclusions to be drawn,
%and a few words on future work.

\section{Nuclear fusion}
\label{sec:intro_fusion}
%\subsection{The process}
In a conventional nuclear power plant, the fuel consists of heavy elements whose nuclei split naturally, forming lighter atoms, in what is known as fission.
%Because this is a spontaneous process, energy is released, which is harnessed to produce electrical power.
Fusion, on the other hand, is the nuclear process where two lighter nuclei combine to form a heavier element.
This does not happen naturally on Earth, but elsewhere in the universe it is commonplace: it is fusion that powers the stars.
This was realised around the 1920s by Sir Arthur Eddington, and the dream of utilising this process for energy production was kindled at the same time~\cite{Wesson2000}.

Though fusion and fission power require very different operating scenarios, the fundamental principle that allows both fusion and fission to occur and release energy is the same.
As can be seen in the curve in figure~\ref{fig:mass_defect}, the mass of an atom is not simply the sum of its nucleons -- its neutrons and protons.
This phenomenon is called the \emph{mass defect}.~\cite{REDBOOK}
As neutrons and protons are added to or subtracted from an element, the combined mass is decreased if the product is closer to the minimum in the curve.
The most common isotope of iron, $\isotope{56}{Fe}$, has been highlighted (\textcolor{OliveGreen}{$\Box$}) in the figure.
As can be seen, iron is at the minimum of the mass curve, meaning that it is the element with the least mass per nucleon.

By Einstein's relation $E=m c^2$ \cite{Einstein1905b} mass is a form of energy, wherefore the decrease in mass is also a decrease in energy, and generally, decreased energy means increased stability.
The tendency of elements combining or falling apart to form more ``iron like'' elements is spontaneous, in the sense that it is statistically more likely to occur than the opposite.
This can be understood from figure~\ref{fig:mass_defect}, by observing that in order to bring an element away from the minimum, mass in the form of energy has to be supplied from somewhere, whereas going toward $\isotope{56}{Fe}$ the excess mass/energy need not fulfil any particular requirements -- it has a whole universe into which it can disperse.
The conservation of energy -- one of the most fundamental principles in all of Physics -- demands that the missing mass be turned into other forms of energy, and it is this energy that is captured in nuclear power plants of both varieties. 
The typical fuel for fusion and fission are represented in figure~\ref{fig:mass_defect} by the two highlighted isotopes $\isotope{2}{H}$ and $\isotope{238}{U}$ (Uranium) respectively.
The $\isotope{2}{H}$ is the isotope of Hydrogen called ``heavy Hydrogen'', which is more commonly known as Deuterium and denoted $\isotope{}{D}$ in fusion science.
As can be seen, the energy that can potentially be gained by fusing light elements is many times greater per nucleon, and hence per kilogramme of fuel, than that which fission yields.\footnote{
The scale in figure~\ref{fig:mass_defect} is in $\unit{eV}$, or \emph{electron Volts}. 
$1\unit{eV}\approx 1.6 \cdot 10^{-19} \unit{J}$, meaning that you would need roughly $2.5 \cdot 10^{19}\unit{eV}$ to heat $1 \unit{g}$ of water $1 \unit{\degrees C}$, but considering that Deuterium atoms are $\sim 1.5 \cdot 10^{26}$ to a kilo, there is still a lot of energy in one nuclear reaction~\cite{Physics}.}
This is one of the reasons why fusion as a power source is so attractive.

That the fusion process is essentially spontaneous does not mean, however, that it is easy to accomplish.
%\subsection{Prerequisites}
\label{sec:prerequisites}
Whereas fission happens spontaneously on Earth, fusion requires much more exotic circumstances.
In fission power plants, the nuclear process is mediated by neutrons, whereas for fusion to occur, the electrostatic repulsion between both the atoms' negatively charged electron clouds, and then between the positively charged nuclei themselves, need to be overcome.
Methods of accomplishing this normally require either extreme temperatures, extreme pressure, or a combination of the two.
At sufficiently high temperature, collisions between atoms will be energetic enough to separate the electrons from the nuclei.
If the frequency of recombining collisions is sufficiently low, a plasma is the result~\cite{Chen1984}, meaning that it is easier to ionise a thin gas than a dense one.
For fusion to occur, however, the resulting ions need to collide with enough force, that they break through the repulsive potential of the nuclear charges, which is many orders of magnitude higher than the repulsion from the electron clouds.
The probability of a nuclear reaction to occur is quantified by the \emph{cross section} for the reaction.
The most favourable cross section for fusion is obtained for the fusion between Deuterium and Tritium ($\isotope{3}{H}$) in the reaction~\cite{Chen1984, REDBOOK}:

\begin{equation}
 \label{eq:fusion}
 \mathrm{D} + \mathrm{T} \equiv \isotope{2}{H} + \isotope{3}{H} \longrightarrow \isotope{4}{He} + \mathrm{n} + 17.6\unit{M eV}
\end{equation}

where $\isotope{4}{He}$ is an ordinary Helium ion, more often referred to as an $\alpha$-particle, and $n$ is a neutron.
The total excess energy from the reaction in equation~\eqref{eq:fusion} is $17.6 \unit{M eV}$, distributed on the fusion products according to their mass, so that the total momentum is conserved, meaning that $\sim 4/5$ of the energy is deposited on the neutron.
Because the neutron is uncharged, it is not confined by the magnetic fields used to contain the plasma, and will therefore leave the core region, depositing it energy in the wall of the plasma chamber, which is how energy will be extracted in a working power plant.
The $\alpha$-particles, on the other hand, will be caught in the magnetic field and, through collisions, deposit their excess energy to the fuel ions, heating the plasma.
Efficient $\alpha$-particle heating therefore is the key to achieving self-sustained nuclear fusion.
Unfortunately, Tritium is not a stable isotope of Hydrogen.
It is radioactive with a half-life of $12.33$ years, and so must be bred, for instance from from Lithium~\cite{Physics, REDBOOK}.

A striking example of fusion in Nature is the sun, which relies on the force of gravity to create the immense pressure needed for the fusion of protons and other elements to occur, which is the process that makes it and all other stars shine.
The circumstances under which fusion can take place are very exotic from an Earthly stand-point, and very difficult to produce in a laboratory.
Creating the conditions of allow for a high enough fusion cross section requires highly specialised devices and knowledge, which is why the engineering and science aspects of fusion research are both very important.

\begin{figure}[t] % fig:mass_defect
 \centering
 \includegraphics[width=\figwidth]{figures/mass_defect}
 \caption[short]{Mass defect for the stable nuclei.
 Deuterium ($\isotope{2}{H}$;~\textcolor{red}{$\bigtriangleup$}) and Uranium ($\isotope{238}{U}$;~\textcolor{Dandelion}{$\pentagon$}) are indicated.
 Spontaneous fusion and fission moves towards the energy minimum ($\isotope{56}{Fe}$;~\textcolor{OliveGreen}{$\Box$}).
 Based on data from \cite{Possel2010} and \cite{REDBOOK}.}
 \label{fig:mass_defect}
\end{figure}

\begin{figure}[t] % fig:configuration
 \centering
 \includegraphics[width=\figwidth]{figures/geometry}
 \caption[illustration of tokamak magnetic fields]{Illustration of the origin of the helical magnetic field lines in a tokamak.
The toroidal (\textcolor{Red}{$\vec{B}_\phi$};~\textcolor{Red}{$\nwarrow$}) and poloidal (\textcolor{Orange}{$\vec{B}_\theta$};~\textcolor{Orange}{$\swarrow$}) contributions to the total (\textcolor{Blue}{$\vec{B}_{tot}$};~\textcolor{Blue}{$\leftarrow$}) magnetic field are indicated.
Neither $\vec{B}_\phi$ nor $\vec{B}_\theta$ exhibit the necessary twist, but their sum $\vec{B}_{tot}$ is a magnetic field whose field lines spiral around the torus.
The safety factor in the figure is $q \approx \frac{r}{R}\frac{B_{\phi}}{B_{\theta}}=0.35$.
The surfaces spanned by the field lines of $\vec{B}_{tot}$ for different (minor) radii are referred to as \emph{flux surfaces}.
In most tokamaks their cross section is not fully circular.
The poloidal magnetic field is induced by the plasma current (\textcolor{OliveGreen}{$\vec{J}$};~\textcolor{OliveGreen}{$\rightturn$}) running along the toroidal axis of the plasma.
Also indicated are the \emph{major} (\textcolor{Black}{$\vec{R}$};~\textcolor{Black}{$\longrightarrow$}) and \emph{minor} (\textcolor{Gray}{$\vec{r}$};~\textcolor{Gray}{$\nearrow$}) radii.}
 \label{fig:configuration}
\end{figure}

\section{Fusion plasmas}
\label{sec:intro_plasma}
\subsection{The fourth state of matter}
\label{sec:fourth_state}
%A plasma is arguably the most complex medium in all of classical physics.
%Just as heating a solid object -- for instance a block of ice, or a metal ingot -- will break the bonds that tie it together causing it to melt, and further heating it will turn into a gaseous state, 
Super-heating or sufficiently depressurising a gas will eventually, through the processes outlined in section~\ref{sec:prerequisites}, lead to the separation of the electrons from the atoms in the material, resulting in an ionised gas -- a plasma.
In analogy with the solid, liquid and gaseous states of matter, plasmas are often referred to as the \emph{fourth state} of matter.

A more rigorous definition of a plasma is that

\begin{quote}
\emph{``[a] plasma is a quasineutral gas of charged and neutral particles which exhibits collective behaviour''}~\cite{Chen1984}, 
\end{quote}

where quasineutral means that the plasma is electrically neutral when viewed from a distance, but may exhibit charge fluctuations on small scales.
In many ways a fluid,\footnote{in physics, the term \emph{fluid} is used for both liquids and gasses, as opposed to solids} plasmas are subject to the already complicated laws of fluid mechanics, but their behaviour becomes even more embroiled by the electromagnetic properties of the plasma, which introduce long range effects not present in other media.
These effects are what lead to collective behaviour in the plasma, and this property is the most important difference between a partly ionised gas and a proper plasma. 
The intermingling of these two areas of physics further implies, that acoustic and electromagnetic waves of all kinds coexist in the plasma, but often on very different time and length scales. 
This makes both analytical and numerical studies of the governing equations very challenging, if one wishes to capture the entirety of this intricate interplay.

Though exotic in many ways, plasmas exist in our everyday surroundings: in fluorescent tubes, neon signs and modern television sets.
The kind of plasmas that occur naturally on Earth are rarer, but not uncommon.
The Norther\footnote{and, of course, Southern} lights and lightning are perhaps the most well known examples.
%They occur when streams of ionised particles from the solar wind get caught by the Earth's magnetic field, colliding with and exciting atmospheric Oxygen and Nitrogen as they spiral towards poles. 
%Another example is lightning strikes, where a large difference in electric potential can temporarily ionise the the air in a violent electrical discharge.
%As the electrons are turned loose, they rush to balance the potential, heating a thin conduit of air to such degree that a plasma forms, which we perceive as a flash of lightning.
Fusion plasmas, however, need to be much hotter in order to have a high enough fusion cross section for fusion power to be feasible.
In order to sustain a fusion grade plasma in a laboratory or a reactor, it needs to be separated from the surroundings; it needs to be confined.

%\subsection{Confinement}
\label{sec:confinement}
In the sun and the stars, the confinement is accomplished by gravity, where the mass of the stellar body is enough to create the pressure needed for the fusion process to be self sustained.
This is not, however, an option for earthbound plasmas.\footnote{consider that Jupiter is in many ways a ``failed'' star: that the mass of the gas giant is too small to ignite its Hydrogen core illustrates the futility of gravitational confinement on Earth}
Instead, research into confining plasmas is divided into two main areas: magnetic and inertial confinement.


\subsection{Confinement -- the ``Tokamak''}
\label{sec:intro_tokamak}
%\subsubsection{Magnetic}
\label{sec:magnetic_confinement}
In the presence of a magnetic field, charged particles will experience a force perpendicular to their velocity and to the magnetic field.
%This is called the \emph{Lorentz force}, and is written
%
%\begin{equation}
% \label{eq:Lorentz}
% \vec{F} = q\vec{v}\times\vec{B},
%\end{equation}
%
%\noindent where $q$ and $\vec{v}$ are the charge and velocity of the particle, and $\vec{B}$ is the magnetic field.
Because the force is always at a right angle to the velocity, it can not lead to an increase in the velocity of the particle, but only change its direction.
This means that the particles will be confined to move in orbits around the magnetic field lines, but they remain free to move parallel to the field.
In order to fully confine the particles, the parallel motion has to be restricted as well.
This can be accomplished by increasing the magnetic field at the edges of the device, creating what is called a ``magnetic mirror''.
Though this will cause many particles to bounce back into the core of the plasma, it can be shown that particle losses at the ends of the device are unavoidable~\cite{Chen1984}.
Therefore, most research into potential power plant designs have been devoted to the study of toroidal magnetic geometries -- instead of tying off the ends of the magnetic field, the field lines are bent into a ring, closing on themselves.
%like a sausage, the field lines are bent into a doughnut shape, closing in on themselves.
The toroidal configuration is illustrated in figure~\ref{fig:configuration}.

Since the ends are eliminated this way, so are the end losses, however, this setup comes with its own difficulties, stemming from the inhomogeneity of the magnetic field.
For a simple toroidal magnetic field, it can be shown that, due to a combination of effects due to the gradient and curvature of the magnetic field, the plasma will tend to be expelled from the core, toward the outside of the torus~\cite{Chen1984, Weiland2000}.
This problem can be solved by introducing a twist (or ``helicity'') in the magnetic field, so that the field lines -- and the particles following them -- spend time on both the in- and the outside of the device.
In that way, the particles pushed toward the edge of the plasma when on one side of the torus, will be pushed back into the core when on the other side.

The twist is created by adding a field in the poloidal direction to the toroidal field; their sum will be a field with field lines spiralling around the torus.
Figure~\ref{fig:configuration} presents an illustration of how the helical field lines are generated.
The first method of inducing this twist utilised external coils for both the poloidal and the toroidal magnetic fields.
The devices were called ``stellarators'', referring to the ambition of reproducing the workings of the sun and her stellar sisters here on Earth.
Though stellarator research is a very active field (see e.g.~\cite{LHD,W7-X}), due to the difficulties of creating a favourable magnetic geometry by external means, most research since the sixties has shifted toward what is known as the ``tokamak'',\footnote{There seems to be some confusion as to where the name ``tokamak'' came from, originally. 
Today, it is most often said to be an acronym for ``\emph{to}roidalnaya \emph{ka}mera s \emph{ma}gnitnaya \emph{k}atushka'', which is Russian for ``toroidal chamber with magnetic coils''~\cite{Wesson2000}.
In older works, however, one can instead read that it originates from ``\emph{to}roidalnaya \emph{kam}era s \emph{ak}sialnym magnitnym polem'', or ``toroidal chamber with axial magnetic field''~\cite{MW}.
While how and why the name came to shift its meaning remains a mystery, both acronyms are suitably descriptive of the device: the tokamak was developed in the Soviet Union in the middle of last century, and is indeed a toroidal chamber, with magnetic coils generating a magnetic field along the toroidal axis of the chamber.
But this is common to all toroidal magnetic confinement devices, and so the name does not cut to the core of what sets the tokamak design apart from the others.}
 a configuration where the twist is accomplished by running a current through the core of the plasma.
The rate of the helicity is measured by a parameter called the \emph{safety factor} ($q$), which can be seen as the number of toroidal turns a magnetic field line make in one poloidal turn.
It is directly proportional to the ratio of the toroidal magnetic field strength ($B_{\phi}$) to the poloidal magnetic field strength ($B_{\theta}$)~\cite{Hazeltine2003}, see figure~\ref{fig:configuration}.
%The tokamak is the device considered in this thesis, and is therefore covered in more detail in section~\ref{sec:intro_tokamak}.
%Though the tokamak has for many years been the brightest shining star of fusion research, stellarators remain an attractive alternative, mainly because they promise an uninterrupted mode of operation, whereas in tokamaks a pulsed mode is the most easily achieved.
%With the advent of super-computing, simulations based of first principles of the complicated stellarator geometries are now fully feasible, though still expensive in terms of computer resources~\cite{GENE}.
%Stellarator research making progress through the Large Helical Device (LHD,~\cite{LHD}) in Japan, and the Wendelstein 7-X (W7-X,~\cite{W7-X}) device in Greifswald, Germany, is currently under construction.
%The aim of the latter is to prove the feasibility of the stellarator as power plant candidate, and operations are scheduled to begin in 2015.
%
%\begin{figure}[t] % fig:coordinates
% \centering
% \includegraphics[width=\figwidth]{figures/coordinates}
% \caption[illustration of toroidal coordinates]{Illustration of a toroidal coordinate system with the \emph{toroidal} (\textcolor{Red}{$\phi$};~\textcolor{Red}{$\leftturn$}) and \emph{poloidal} (\textcolor{Orange}{$\theta$};~\textcolor{Orange}{$\leftturn$}) angles, and the \emph{major} (\textcolor{Black}{$R$};~\textcolor{Black}{$\longrightarrow$}) and \emph{minor} (\textcolor{Blue}{$a$};~\textcolor{Blue}{$\nearrow$}) radii indicated.
%The minor radius is usually called $r$, when treated as a free parameter.}
% \label{fig:coordinates}
%\end{figure}
%

%\subsubsection{Other}
%The most common method of inertial confinement is often called laser fusion.
%A small pellet of Deuterium fuel is bombarded from all direction with high-power lasers, vapourising the shell of the pellet, and thereby sending a shock wave into the fuel, compressing it.
%The result is, in essence, a miniature fusion bomb.
%Both the lasers and the fuel pellet need very high precision, since any asymmetries tend to breed instabilities that let the high pressure escape, much like a balloon escapes between ones fingers when squeezed.
%The time scales involved in laser fusion are much shorter, and the pressures considerably higher, than for magnetic confinement, meaning that the engineering aspects of this mode of fusion are quite different from those involved in magnetic fusion, though the same fundamental equations govern the behaviour of the resulting plasma in both cases.
%
%Other inertial and hybrid confinement schemes exist, but are less studied.



%\subsection{Configuration}
%As briefly outlined in section~\ref{sec:magnetic_confinement}, the toroidal field is not enough, it needs a twist for the plasma to become stable.
In the tokamak design, instead of relying on external coils to crate this twist, an axial current is induced in the plasma, creating the poloidal field through Ampere's law~\cite{Physics}.
This is done by treating the plasma -- a very good conductor due to the free mobility of the electrons -- as a the secondary winding of a transformer, thus
%By driving a strong current through the primary winding, a magnetic flux is induced in a transformer core encircling the plasma chamber, in turn 
inducing a current in the plasma.\footnote{this also helps to heat the plasma through resistive (or ``ohmic'') heating, though at high temperatures the plasma is too good a conductor for this to be the only source of heating power}
The relationship between the plasma current and the helicity of the magnetic field lines is illustrated in figure~\ref{fig:configuration}.

A drawback of this method of introducing a helical twist in the magnetic field is that the electromagnetic field driving the plasma current is proportional to the change in the magnetic flux, as described by Faraday's law of induction~\cite{Physics}.
Therefore, the current can only be induced as long as the magnetic flux increases, which it cannot do indefinitely.
Eventually the transformer core will saturate, meaning that the plasma current can no longer be sustained.
Though there are advanced operating scenarios under investigation that may circumvent this, tokamak operations are currently limited to pulsed mode.
This is not a problem for the study of plasma dynamics, which usually involve time scales much shorter than the pulse time, but certainly a drawback when it comes to efficient power production.

%That economically viable fusion is hard to accomplish does, however, have one advantage: safety.
%The fusion process is inherently safe, in that there are no physical processes in a fusion device, that can lead to uncontrolled nuclear reactions, such as may happen with fissile materials.
%There are several reasons for this.
%First of all, there is only very little fuel in the reactor chamber at any given time, but even if fuel should somehow leak into the core, it would not cause a runaway reaction due to the stability properties of the plasma.
\subsection{Stability and quality of the confined plasma}
\label{sec:stability}
Plasma confinement is a precarious process, only possible for precisely tuned parameters.
One such parameter is the so called plasma $\beta$, which expresses the ratio of the particle pressure to the confining magnetic pressure.
This parameter cannot supersede a few percent in tokamaks, or the plasma will be subject to large scale instabilities and disrupt, losing confinement almost at once~\cite{Hazeltine2003, Weiland2000}.
Pressure is related to the particle density and the temperature through the Boltzmann constant: $p=n_e k_B T$~\cite{Physics}.\footnote{the subset $_e$ is for \emph{electrons}, which is conventionally used, since the electron density in a fully ionised gas is a measure of the ion density, regardless of the number of different ion species}
Therefore, an increase in either the number of particles or temperature would proportionally increase the pressure, eventually bringing it above the $\beta$-limit.
%This can be motivated by the ideal gas law, which says that the preassure is proportional to both the particle density and the temperature:
%
%\begin{equation}
%\label{eq:gaslaw}
%p V = N_e k_B T.
%\end{equation}

%Here $p$ is the preassure, $T$ is the temperature, $N_e$ is the number of particles in the gas
Though the $\beta$-limit constrains puts a severe constraint on the operating regimes available to achieve fusion power, it also makes the process inherently safe from anything like a nuclear melt-down.
``Disruption'' and ``loss of confinement'' may sound dire enough, considering that the temperature of the plasma can reach in excess of a hundred million degrees Celsius.
The actual energy content of the plasma, however, is very modest, which can also be seen from the definition of pressure.
%Both sides of equation~\eqref{eq:gaslaw} have the dimension of energy, meaning that dividing both sides by the volume gives the energy density.
Dimensionally, pressure is a measure of the energy density in a fluid.
For a typical fusion plasma in ITER~\cite{ITER}, the particle density will be $n_e\approx 10^{20}\unit{m^{-3}}$ for a volume of $V\approx10^3\unit{m^3}$, and the temperature will be roughly $T\approx10^{8}\unit{K}$.
With $k_B\approx 10^{-23}\unit{J/K}$, combining these gives an energy density of approximately $10^5 \unit{J/m^3}$.
This is equivalent to $10^3\unit{k Pa}$, which is of the same order as the normal atmospheric pressure at sea level.\footnote{the energy associated with the free electrons has been neglected here: the ionisation energy for Hydrogen is $\sim 10\unit{eV}$, which translates to roughly $10^2\unit{J/m^3}$, so this contribution is negligible compared to the thermal energy of the ions}

Based on the energy content of the plasma, a measure of the quality of the confinement can be defined as the quotient of the energy content $E$ and the power input $P_{in}$ needed to sustain the plasma at that level of energy: $\tau_E \equiv E/P_{in}$, which has the dimension time.
This is a measure of how quickly the energy would be lost, if power were not supplied, and is therefore called the \emph{energy confinement time}.
By dimensional arguments it can be shown that the power balance leads to a requirement for net energy production of $\tau_E n_e > (\tau_E n_e)_c$, for some critical value $(\tau_E n_e)_c$~\cite{Hazeltine2003}.
This is called the \emph{Lawson criterion}, and expresses the condition for power \emph{break-even}.
A more concrete performance parameter is that of the ``fusion triple product'': $n_e T \tau_E > (n_e T \tau_E)_c$, valid in the temperature range considered for fusion.
The triple product is a condition for a self sustained plasma, meaning that the necessary power to heat the plasma comes from the $\alpha$ particles generated by fusion events.\footnote{both the Lawson criterion and the fusion triple product are valid measures of the quality of fusion plasmas for magnetically as well as initially confined fusion plasmas, but in magnetic confinement fusion $n_e$ is typically small and $\tau_e$ large, while the opposite holds for inertial confinement fusion}
In fusion experiments, the achieved triple product has been increasing exponentially over time since the dawn of fusion research, and the ITER device currently under construction is expected to continue this trend, with an estimated output power of five to ten times the input power~\cite{Weiland2000, REDBOOK, ITER}.
Because of stability requirements, such as the $\beta$-limit mentioned above, the particle density and temperature are both restricted.
Therefore, significantly increasing the Lawson parameter or the fusion triple product requires increasing the energy confinement time, which requires an understanding of the transport mechanisms at work.


\section{Plasma impurities}
\label{sec:intro_impurities}
Impurities -- any ions that are not part of the fuel -- tend to dilute the fuel, making collisions that produce fusion rarer, and thus reducing the fusion power.
Heavier elements also tend to cool the plasma through radiative processes.
Their high nuclear charge make them hard to ionise fully, even at the temperatures of a fusion plasma, and the electrons remaining bound to the impurity can then, rather than separate from the nucleus, respond to a collision by jumping to a higher electron orbit~\cite{Harte2010}.
%This is only a temporary shift, however, and the ion will soon relax, the excited electrons jumping back down to the lower energy levels.
As the electrons relax, returning to the lower energy levels, they lose the energy gained in the collision, which is released in the form of photons.
This is called \emph{line radiation}, because the frequencies of the released photons correspond to lines in the light spectrum characteristic to the element that produced them.
Since some heavy elements may never be fully ionised, line radiation can continue indefinitely.
Therefore, even a small dilution of heavy impurities, can lead to significant energy losses in the plasma.

There are mainly three potential sources of impurities: the first being the walls of the reactor chamber.
Due to the different roles played by different parts of the walls, they contribute both light and heavy impurities.
The divertors, for instance, need to withstand the heavy power loads from energetic particles, and are therefore made of heavy metals such as Tungsten (W; nuclear charge $Z=72$).
Because of the danger of line radiation, using an element as heavy as Tungsten is not practical for all of the chamber, and hence lighter candidates with high heat resilience are used elsewhere.
For example, at the Joint European Torus (JET,~\cite{JET}) the new ITER-like wall project was recently initiated, testing the feasibility of using a coating of the light metal Beryllium (Be, nuclear charge $Z=4$) on the plasma facing first wall of the reactor chamber~\cite{Matthews2009}.

Not all impurities, however, are contaminants.
The second main source of plasma impurities is injections of particles for control purposes.
Here the cooling mechanisms are beneficial to the operation of the fusion reactor.
By injecting elements such as Argon (Ar, $Z=18$) that radiate energy in the right locations, the heat load on components such as the divertors can be spread out, protecting them from wearing out~\cite{Loarte2011}.
Impurities are also injected for experimental purposes, in order to study their transport properties.

Finally, the fuel ions will, in a working power plant, be diluted by the steady production of $\alpha$-particles (sometimes referred to as ``Helium ash'') through fusion reactions.

Of major concern is whether different kinds (or ``species'') of impurities will experience an inward or an outward pinch.
Simulations of this is the main topic in this thesis.

\section{Transport processes}
\label{sec:intro_transport}
%\subsubsection{Classical transport}
Understanding the transport of particles, heat, momentum etc. in fusion plasmas is a very important topic of research.
As mentioned above, controlling the transport properties of the plasma may be the only way forward when it comes to increasing the fusion efficiency, as measured by the Lawson parameter and the fusion triple product (section~\ref{sec:stability}).

In fluids, it is common to describe the transport as consisting of diffusion and convection.\footnote{advection is often used in place of convection, using convection to mean the sum of advective and diffusive transport}
Diffusion is the (seemingly) random spreading of a quantity in a fluid.
It can often be understood as being mediated by collisions in the flow leading to a dispersive random walk~\cite{Einstein1905a}.
Diffusive transport is driven by gradients, and so diffusion is directed from areas of abundance, to areas of scarcity.
Thereby diffusion tends to even out profiles of temperature, density etc.

The convective part of the transport relates to bulk motion of particles in a fluid.
It can either be up or down gradient, depending on the situation.
In fusion plasmas, an net convective velocity is often referred to as a ``pinch''.

Transport is often separated into \emph{classical} and \emph{anomalous} transport, where classical refers to transport dominated by collisions, whereas all other observed transport is termed anomalous~\cite{Liewer1985}.
%\subsubsection{Anomalous transport}
\label{sec:intro_turbulence}
%It has been said that Nature abhors a vacuum\footnote{this saying has been attributed to many thinkers, including Baruch Spinoza, Ren\'e Descartes, and Fran\c{c}ois Rabelais -- it may even have been coined in ancient times, by Aristotle}.  
%A more general statement to similar effect might be: 
%strong gradients tend to make physicists' lives very complicated. 
Experiments have shown that, for most regions of the fusion plasma, anomalous transport clearly dominates over classical transport~\cite{Liewer1985}.
The most common example of anomalous transport in plasmas is turbulent transport.
Turbulence is often associated with strong gradients, which represent free energy within a system that can drive drive instabilities. 
It is difficult to envision any classical situation, where the gradients are more pronounced than in a modern magnetic confinement fusion device, however, turbulence is a very common phenomenon in all of Nature.
Hence it has been a topic of study for scientists and engineers for the better part of three hundred years, but its nonlinear character means that studying the effects of turbulence is very challenging.
One main feature of turbulence is the interaction and interchange between different time and length scales, meaning that turbulent transport cannot be properly described by simple convection and diffusion.
Locally, however, this approximation can be valid, when looking at space and time averaged fluxes~\cite{Angioni2006, Nordman2007a, Nordman2008, Nordman2011}.
The turbulent transport then manifests itself as effective diffusivities and pinches, at different minor radii.

All turbulent dynamics exhibit nonlinearities, making exact analytical solutions to equations of motion hard to come by, necessitating the application of numerical methods.
To aid researchers when predicting and interpreting experimental outcomes, numerical tools have been developed to study the transport.
Both dedicated transport solvers and more general plasma codes are used to this end, and one main topic in this thesis is comparing results from two such models: a multi-fluid and a gyrokinetic model.

