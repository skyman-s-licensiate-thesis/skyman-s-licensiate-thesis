\chapter{Turbulent impurity transport}
\label{ch:transport}

\section{TE and ITG mode turbulence}
\label{sec:trans_turbulence}
The turbulence in magnetically confined fusion plasmas, such as those in Tokamaks like the proposed ITER device~\cite{ITER}, has important and non-trivial effects on e.g. the quality of the energy confinement -- effects that are hard to tackle both analytically and numerically. 
The problem of transport of energy and particles in a Tokamak plasma is an area of research where turbulence plays a major role, and that is intimately associated with the performance of future fusion reactors.

Many types of instabilities that exhibit this behaviour can be explained as analogous to the Rayleigh--Taylor instability, where a dense fluid is supported by a less dense fluid against the influence of gravity~\cite{Chen1984, Hazeltine2003, Weiland2000}.
This is the case on the outboard side of the tokamak, which for its propensity for driving instabilities is called the \emph{bad curvature region}.
The profiles of the density and temperature perturbations will therefore have maxima on the outside, and minima on the inside, which is called \emph{ballooning}~\cite{Hirose1994, Weiland2000}.

The origin of turbulent transport in tokamak plasmas is the fluctuations in the electric and magnetic fields.
Crucially, the magnitude of the transport does not only depend on the the magnitude of the fluctuation, but also on the extent of the phase correlation between the fluctuating quantities.
For instance, for a net particle flux, the fluctuation in the velocity field needs to be accompanied by a fluctuation in the particle density that is correlated with the velocity fluctuation.

For the linear modes driving the turbulence considered in this thesis -- ion temperature gradient (ITG) and trapped electron (TE) modes in low $\beta$ plasmas -- the perturbation can can be considered to be mainly electrostatic.
Both the ITG~and the TE~mode are examples of so called reactive drift wave modes.
They are both associated with length scales known to cause transport ($k_\theta\rho_s\approx 0.3$), and their mode frequencies are of the same order as the magnetic and diamagnetic drift frequencies ($\omega_r \sim \omega_D,\omega_*$).

The ITG mode can be understood as arising from a fluctuation in the temperature distribution, which under the influence of the poloidal magnetic drift causes a response in the ion density.
If the electrons are considered adiabatic, they will respond by quickly redistributing according to the new landscape, creating an electric potential difference between the compressible ions, and the thermal electrons.
The resulting electric field ($\vec{E}$) will, in turn, lead to a drift velocity perpendicular to $\vec{E}$ and to the magnetic field ($\vec{B}$) -- the $\vec{E} \times \vec{B}$ drift -- acting on the temperature perturbation, and thus closing the feedback loop.
In the bad curvature region, this feedback will be positive leading to an instability.
The origin of the TE mode instability is similar in nature to the origin of the ITG mode~\cite{Weiland2000}.

In the case of a purely electrostatic perturbation, the particle flux of ion species $j$ can formally be written~\cite{Liewer1985}:

\begin{equation} \label{eq:Gamma_derivation}
\Gamma_{j} = \left<\delta n_j v_E \right>,
\end{equation}

where $\delta n_j$ is the perturbation in the density of species $j$ and $v_E$ is the $\vec{E}\times\vec{B}$ drift velocity~\cite{Weiland2000}. 
The angled brackets imply a time and space average over all unstable modes. 

%To arrive at a set of equations describing the impurity transport that are both meaningful and solvable, some approximations are necessary. 
%The advances in high performance computing have allowed fusion modellers to move from fluid descriptions of the plasma to kinetic descriptions as the basis for turbulence modelling, however, the underlying physics are easier to grasp from fluid models.

Deriving expressions for the drift velocities and the density response etc. can be done using  different theoretical frameworks.
In kinetic theory the plasma is described through distribution functions of velocity and position for each of the included plasma species.  
Hence, kinetic equations are inherently six-dimensional, however, in magnetically confined fusion plasmas the confined particles are generally constrained to tight orbits along field lines.  
This motivates performing an average over the gyration, reducing the problem to five-dimensional gyrokinetic equations~\cite{Antonsen1980,Frieman1982,Hahm1988,Brizard1989}.
Since the equations governing the evolution of the distributions are all coupled, the resulting decrease in numerical complexity is considerable.
Fluid theory, on the other hand, is derived by taking the moments of the kinetic equations to some order, making them tractable by finding an appropriate closure~\cite{Weiland2000}.
In addition to making the workings of the plasma more accessible, by reintroducing familiar physical concepts such as pressure and density, fluid models are also several orders of magnitude more computationally efficient. 

Whether $\delta n_j$ and $v_E$ are derived from fluid or gyrokinetic theory, performing the average in equation~\eqref{eq:Gamma_derivation} for a fixed length scale $k_\theta\rho_s$ of the turbulence, leads to an expression of the following form:

\begin{equation}
 \label{eq:transport}
 \frac{R\Gamma_j}{n_j} = D_j\frac{R}{L_{n_j}} + D_{T_j}\frac{R}{L_{T_j}} + R V_{p,j}.
\end{equation}

The first term in equation~\eqref{eq:transport} corresponds to diffusion, the second to the thermodiffusion and the third to the convective velocity (pinch), where $R/L_{X_j}=-R\grad X_j/X_j$, with $X=n,\,T$, are the normalised logarithmic gradients of density and temperature for species $j$, and $R$ is the major radius of the tokamak.
The pinch here contains contributions from curvature and parallel compression effects, however, the thermodiffusive term in equation~\eqref{eq:transport} is sometimes referred to as the thermopinch and included in the convective velocity, so as not to confuse it with the proper (i.e. density gradient driven) diffusion. 
These terms have been described in detail in previous work, see e.g.~\cite{Nordman2007a, Nordman2008, Angioni2006} and~~\ref{paper:thefirstpaper} and~\ref{paper:thesecondpaper} in this thesis.

\section{Impurity transport}
\label{sec:PF}
For trace impurities, equation~\eqref{eq:transport} can be uniquely written as a linear function of $\grad n_Z$, offset by a convective velocity or ``pinch'' $V_Z$:

\begin{equation} \label{eq:transport_short}
\Gamma_{Z} = 
-D_Z\grad n_Z + n_Z V_Z \Leftrightarrow \frac{R\Gamma_Z}{n_Z} =
D_Z\frac{R}{L_{n_Z}} + RV_Z,
\end{equation}

where $D_Z$ is the impurity diffusion coefficient, and $V_Z$ is the impurity convective velocity with the thermopinch included.
Both the $D_Z$ and $V_Z$ are independent of $\grad n_Z$ in the trace impurity limit~\cite{Angioni2006}.
$Z$ refers to the charge number of the impurity.

In the core of a steady-state plasma with fuelling from the edge (i.e. no internal particle sinks or sources), the impurity flux $\Gamma_Z$ will go to zero.
The zero-flux impurity density gradient (peaking factor) is defined as 

\begin{equation}
 \label{eq:PF}
 PF_Z=-\frac{R\,V_Z}{D_Z},
\end{equation}

for the value of the impurity density gradient that gives zero impurity flux.\footnote{this number is also sometimes referred to the \emph{Péclet number}~\cite{Arter1995, Bakunin2003}}
Solving the linearised equation~\eqref{eq:transport_short} for $R/L_{n_Z}$ with $\Gamma_Z = 0$ yields the interpretation of $PF_Z$ as the gradient of zero impurity flux, and it quantifies the balance between convective and diffusive impurity transport.
Specifically, the sign of the peaking factor is determined by the sign of the pinch, meaning that $PF>0$ is indicative of a net inward impurity pinch, giving a peaked impurity profile.
Conversely, if $PF<0$ the net impurity pinch is outward, leading to a hollow impurity profile.
The latter condition is called a \emph{flux reversal}, and conditions leading to this are of particular interest, since an accumulation of impurities in the core of the plasma is preferably to be avoided (see section~\ref{sec:intro_impurities}).
The relationship of $PF$ to $D_Z$ and $V_Z$ is illustrated in figure~\ref{fig:Gamma}. 

Much of the observed difference between the TE~and ITG~mode dominated cases -- a major topic in the appended articles -- can be understood from the convective velocity $V_Z$ in equation~\eqref{eq:transport_short}.
Particularly, the pinch contains two terms that depend on the impurity charge number $Z$~\cite{Angioni2006}:

\begin{itemize}
 \item thermodiffusion (thermopinch):
 \begin{itemize}
  \item $V_{\grad T_Z}\sim \frac{1}{Z}\frac{R}{L_{T_Z}}$,
  \item inward for TE mode $\left(V_{\grad T_Z}<0\right)$, outward for ITG mode $\left(V_{\grad T_Z}>0\right)$,
 \end{itemize}
 \item parallel impurity compression: 
 \begin{itemize}
  \item $V_{\parallel,Z}\sim\frac{Z}{A_Z}k_\parallel^2\sim\frac{Z}{A_Z q^2}\approx\frac{1}{2 q^2}$,
  \item outward for TE mode $\left(V_{\parallel_Z}>0\right)$, inward for ITG mode $\left(V_{\parallel_Z}<0\right)$.
 \end{itemize}
\end{itemize}

Here $1/k_\parallel$ decides the wavelength of the parallel structure of the turbulence.
Due to the ballooning character of the modes considered, this is proportional to the safety factor ($q$).
%For TE~modes, $\widetilde{\omega}_r>0$, whereas for ITG~modes $\widetilde{\omega}_r<0$~\cite{Weiland2000}.
The $Z$ dependence in the parallel impurity compression is expected to be weak, since the mass number is approximately $A_Z \approx 2Z$ for an impurity species with charge $Z$.
The thermodiffusive contribution, however, can dominate the transport for low $Z$ impurities (such as the Helium ash). %, thereby qualitatively explaining the observed $Z$ scalings of the impurity peaking factor.
The direction of these contributions to the pinch are governed mainly by the considered mode's drift direction, which is different for TE~and ITG~modes~\cite{Weiland2000}.%real frequency ($\widetilde{\omega}_r$).

