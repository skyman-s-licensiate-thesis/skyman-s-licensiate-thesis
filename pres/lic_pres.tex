\documentclass[c,svgnames]{beamer}
%\documentclass[c,svgnames,handout]{beamer}

% This file must start like this if you want to use beamer script.
% The option c can be changed to t if you want things to be placed at
% the top of slides.


%= Extra packages:
%
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}   % Accept european-encoded (latin1) characters.
\usepackage{bookman}
\usepackage{subfigure}
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fixmath}
\usepackage{todonotes}
\newcommand{\TODO}[1]{\todo[inline]{#1}}
\usepackage{xcolor}

%= Graphics
%
% The package graphics is used inside the style file and must be added.
% The graphics path must be set in such way that the file Logoneg.eps 
% is found, which is the used logo.
%
\usepackage{graphics}
\graphicspath{{./figures/}}
\DeclareGraphicsExtensions{.pdf, .png, .eps, .ps}

%= Set layout of talk
%
\usepackage{beamercthshorttalk}
\definecolor{secretwhite}{rgb}{1.0,0.99,.98}
\input{CommandDefinitions.tex}
\input{cc_beamer.tex}
\renewcommand\mathfamilydefault{\rmdefault}
\newcommand{\mycite}[1]{\ensuremath{^{\text{[#1]}}}}



%= Define talk
% 
% The command \formatdate is style specific, to get a nice frame around
% name of workshop/conference. You can try to remove it.
%
\title[Turbulent impurity transport]{Turbulent impurity transport\\in tokamak fusion plasmas}
\subtitle{\emph{Presentation of licentiate thesis}\\{\scriptsize Discussion leader: Dr Yueqiang Liu}}
\date[17/1 2012]{\formatdate{Licentiate seminar, 17 January 2012}}
%
\author[A. Skyman]{Andreas Skyman}
\institute{\\[-3ex]
            \texttt{andreas.skyman@chalmers.se}\\
            Department of Earth and Space Sciences\\
            Chalmers University of Technology\\[2ex]
            \CcGroupBySa{0.618}{2ex}\\[1ex]
            {\tiny\CcNote{\CcLongnameBySa}}
            \vspace*{-7.0ex}
}

\beamertemplatetransparentcovered

%= Include this to start directly in full screen mode
%
\mode<beamer> %------------------------------------------------------------
{
  %=== Extra PDF commands
  %
  \hypersetup{pdfpagemode=FullScreen}  
}



%= Start of document

\begin{document}


%= Title page
%
\cthtitlepage



%= These commands add stuff to the footer
%
% See beamer manual for other choices, such as page number
%
\mode<beamer>{
\addfootbox{blackbackground}{\tiny \hfill\insertshorttitle: \insertsubsection~(\insertframenumber/\inserttotalframenumber)}
%\addfootbox{blackbackground}{\hfill\insertshortauthor\rule{2mm}{0mm}}
}



%= Uncomment this if you want to include the navigation bar
%
%\setbeamertemplate{navigation symbols}[vertical]


% Outline maybe not needed if talk is very short

\section*{Outline}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}
\subsection{Fusion -- a brief introduction}

\begin{frame}
    \frametitle{Fusion and energy}
    \begin{columns}[c]
        \begin{column}{6.0cm}
            Nuclear fusion powers the stars:
            \begin{itemize}
                \item<2-> Fusion of light elements $\Longrightarrow$ excess energy
                \item<3-> Fuel for fusion:
                \begin{itemize}
                    \item<3-> Deuterium
                    \item<3-> Tritium
                \end{itemize}
                \item<4->{\small
$\underbrace{\isotope{2}{H} + \isotope{3}{H}}_{D+T} \longrightarrow \isotope{4}{He} + n + 17.6\unit{M eV}$
                }%
                \item<5-> Extreme conditions $\Longrightarrow$ \emph{plasmas}
        \end{itemize}
        \end{column}
        \begin{column}{6.0cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{mass_defect} \\
                mass defect \mycite{Krane, 1988; Pössel, 2010}
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Magnetic confinement}
\begin{frame}
    \frametitle{Confining plasmas}
    A plasma is\ldots\\
    \begin{quote}
        \emph{``\ldots a quasineutral gas of charged and neutral particles which exhibits collective behaviour''} \mycite{Chen, 1984}
    \end{quote}
    \uncover<2->{Examples: the Sun, polar lights, flourescent tubes, \emph{fusion~plasmas}}\\
    ~\\
    \uncover<3->{Fusion requires confining the plasma:}
    \begin{itemize}
        \item<3-> Charged particles\only<1-3>{ orbit}\only<4->{\emph{: confined} to} magnetic field lines 
        \item<5-> Parallel motion: constrained by closing fieldlines
        \item<6-> $\Longrightarrow$ \emph{Toroidal} magnetic field
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Toridial devices}
    A short introduction to toroidal geometry:
    \begin{columns}[c]
        \begin{column}{6cm}
            \begin{itemize}
                \item<2-> major radius (\textcolor{Black}{$R$})
                \begin{itemize}
                    \item<2-> \emph{inboard} $\longrightarrow$ \emph{outboard}
                \end{itemize}
                \item<3-> minor radius (\textcolor{Blue}{$a$})
                \begin{itemize}
                    \item<3-> \emph{core} $\longrightarrow$ \emph{edge}
                \end{itemize}
                \item<4-> toroidal angle (\textcolor{Red}{$\phi$})
                \begin{itemize}
                    \item<4-> \emph{long} way around
                \end{itemize}
                \item<5-> poloidal angle (\textcolor{Orange}{$\theta$})
                \begin{itemize}
                    \item<5-> \emph{short} way around
                \end{itemize}
            \end{itemize}
         \end{column}
        \begin{column}{6cm}
            \begin{center}
                \includegraphics[width=0.99\textwidth]{tokamak}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Toridial devices -- the \emph{tokamak}}
    Tokamaks are the most common toroidal devices
    \begin{columns}[c]
        \begin{column}{6cm}
            \uncover<2->{Twist in magnetic field comes from \emph{plasma current}:}
            ~\\
            \begin{itemize}
                \item<3-> \emph{total} field (\textcolor{Blue}{$\vec{B}_{tot}$})
                \begin{itemize}
                    \item<3-> \emph{helical} field lines
                \end{itemize}
                \item<4-> \emph{poloidal} field (\textcolor{Orange}{$\vec{B}_\theta$})
                \begin{itemize}
                    \item<4-> \emph{internal} current (\textcolor{Green}{$\vec{J}$})
                \end{itemize}
                \item<5-> \emph{toroidal} field (\textcolor{Red}{$\vec{B}_\phi$})
                \begin{itemize}
                    \item<5-> \emph{external} coils
                \end{itemize}
                \item<6-> current also \emph{heats} plasma
            \end{itemize}
         \end{column}
        \begin{column}{6cm}
            \begin{center}
                \includegraphics[width=0.99\textwidth]{tokamak_2}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Plasma instabilities}
    \begin{columns}[c]
        \begin{column}{7cm}
            \uncover<1->{Intabilities due to gradients:}
            \begin{itemize}
                \item<2-4> inboard side -- \emph{good curvature}
                    \begin{itemize}
                        \item<3-> \emph{light} fluid on \emph{heavy} fluid
                    \end{itemize}
                \item<4-> outboard side -- \emph{bad curvature}
                    \begin{itemize}
                        \item<4-> \alert<5->{\emph{heavy} fluid on \emph{light} fluid}
                    \end{itemize}
                \item<5-> \emph{Rayleigh--Taylor instability}
            \end{itemize}
            \uncover<6->{Modes driven by:}
            \begin{itemize}
                \item<7-> Ion Temperature Gradient (ITG)
                \item<8-9> Electron Temperature Gradient (ETG)
                \item<9-10> Trapped Electrons (TE)
            \end{itemize}
        \end{column}
        \begin{column}{4.5cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{JET_RT}\\
                \mycite{JET/EFDA}
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{The ITG mode in brief}
%    \TODO{FIX THIS}
%    Some more about the feedback responisble
%\end{frame}

\subsection{Plasma impurities}
\begin{frame}
    \frametitle{Origins of impurites}
    \begin{columns}[c]
        \begin{column}{7cm}
            Impurities: ``species'' that are not \emph{fuel}

            ~

            \uncover<2->{Three main sources:}
            \begin{itemize}
                \item<3-> plasma facing surfaces \\(W, Be, C\ldots)
                \item<4-> injected (Ar, Ne\ldots)
                \item<5-> in ITER: helium ``ash''
            \end{itemize}
        \end{column}
        \begin{column}{4.5cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{JET}\\
                \mycite{JET/EFDA}
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Effects of impurites}
    \begin{columns}[c]
        \begin{column}{7cm}
            Impurities can be detrimental to fusion power:

            \begin{itemize}
                \item<2,4-> dilution of plasma \visible<4->{--\\ fewer fusion reactions}
                \item<3,5-> (line-)radiation of energy \visible<5->{--\\ sometimes desirable}
            \end{itemize}
            ~\\
            \uncover<6->{Impurities are sometimes useful, sometimes a nuisance\ldots}\\
            ~\\
            \visible<7->{Needs \emph{understanding}}
        \end{column}
        \begin{column}{4.5cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{JET}\\
                \mycite{JET/EFDA}
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Turbulence in plasmas}
\begin{frame}
    \frametitle{Studying plasma turbulence -- why?}
    Energy \emph{``break-even''}:\\ \emph{produced} energy $\ge$ \emph{input} energy 

    ~\\

    \uncover<2->{Condition for break-even:}
    \begin{itemize}
        \item<2-> The ``fusion tripple-product''
        \begin{itemize}
           \item<2-> \emph{density} $\times$ \emph{confinement time} $\times$ \emph{temperature} 
           \item<3-> $n \, \tau_E \, T > \text{critical value}$
        \end{itemize}
        \item<4-> $n$ and $T$ as good as they get\ldots
        \item<5-> $\tau_E$ depends on transport\ldots
        \item<6-> turbulence dominates transport
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Studying plasma turbulence -- ok, how?}
    Challenge: turblence is nonlinear

    \begin{itemize}
        \item<2-> analytical methods:
        \begin{itemize}
            \item<2-> theory \emph{is} well developed, however\ldots
            \item<3-> \ldots quantitative results needed
        \end{itemize}
        \item<4->computer models:
        \begin{itemize}
            \item<4-> fluid models (efficient)
            \item<5-> gyrokinetic models (``more'' physics)
            \item<6-> (still a challenge\ldots)
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Gyrokinetic models}
    \begin{columns}[c]
        \begin{column}{6cm}
            How make simulations less demanding? \visible<2->{-- \emph{approximate!}}

            \begin{itemize}
                \item<3-> particle gyrations are
                \begin{itemize}
                    \item<3-> \emph{fast} (high frequency)
                    \item<4-> \emph{small} (compared to system)
                    \item<5-> motivates gyro-\emph{averaging} 
                \end{itemize}
                \item<6-> $6 \longrightarrow 5$ dimensions
                \item<7-> a significant gain\visible<8->{\ldots}
                \visible<8->{\item<8-> \ldots but still \only<8>{expensive}\only<9->{\alert{expensive}!}}\\
                \visible<9->{(\emph{millions} of computer hours\ldots)}
            \end{itemize}
        \end{column}
        \begin{column}{6cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{contour}\\
                electro static fluctuations
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Turbulent particle transport}
    What to \emph{do} with the data?
    \begin{columns}[c]
        \begin{column}{6.0cm}
            Quantifying turbulent transport:
            \begin{itemize}
                \item<2> fluxes ($\Gamma_Z$) are obtained from simulations
                \item<3-> Contributions to transport:
                \begin{itemize}
                    \item<4-> diffusion
                    \item<5-> convection (``pinch'')
                    %\item<5-> $\frac{R\Gamma_j}{n_j} = \underbrace{D_j\frac{R}{L_{n_j}}}_{\text{diffusion}} + \underbrace{D_{T_j}\frac{R}{L_{T_j}} + R V_{p,j}}_{\text{pinch}}$
                \end{itemize}
                \item<6-> For impurities (locally):
                \begin{itemize}
                    %\item<6-> $\underbrace{\frac{R\Gamma_Z}{n_Z}}_{\text{flux}} = \underbrace{D_Z\frac{R}{L_{n_Z}}}_{\text{diffusion}} + \underbrace{RV_Z}_{\text{pinch}}$
                    \item<6-> $\underbrace{\widetilde{\Gamma}_Z}_{\text{flux}} = \underbrace{D_Z\frac{R}{L_{n_Z}}}_{\text{diffusion}} + \underbrace{RV_Z}_{\text{pinch}}$
                \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{6.0cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\linewidth]{timeseries_lic} \\
                \uncover<6->{scalar quantities obtained as time-averages}
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Impurity ``peaking factors''}
    %Impurity transport equation: $\frac{R\Gamma_Z}{n_Z} = D_Z\frac{R}{L_{n_Z}} + RV_Z$
    Impurity transport equation: $\widetilde{\Gamma}_Z = D_Z\frac{R}{L_{n_Z}} + RV_Z$
    \begin{columns}[c]
        \begin{column}{6.0cm}
            \uncover<2->{Solve for $R/L_{n_z}$ at zero flux:}
            \uncover<2->{
            \begin{equation*}
                \frac{R}{L_{n_Z}} = -\frac{RV_Z}{D_Z} \equiv PF
            \end{equation*}
            }
            \uncover<3->{The ``peaking factor'' ($PF$):}
            \begin{itemize}
                \item<3-> \emph{gradient} of zero impurity flux
                \item<4-> quantifies balance of \emph{diffusion} and \emph{convection}
                \item<5-> the sign is important:
                \begin{itemize}
                    \item<6-> $PF > 0$ $\Longrightarrow$ \emph{inward} pinch
                    \item<7-> $PF < 0$ $\Longrightarrow$ \emph{outward} pinch
                \end{itemize}
            \end{itemize}
            
        \end{column}
        \begin{column}{6.0cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\linewidth]{Gamma} \\
                finding the peaking factor ($PF$) from the impurity flux ($\Gamma$)
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\section{Articles}
\subsection{Paper I}

\begin{frame}
    \frametitle{\small Fluid and gyrokinetic simulations of impurity transport at JET \mycite{Nordman, Skyman, Strand et al.; \emph{Plasma Physics and Controlled Fusion}, 2011}}
    Three models applied to JET experiments

    \emph{Ion Temperature Gradient} (ITG) mode turbulence

    \begin{columns}[c]
        \begin{column}{6.5cm}
            \begin{itemize}
                \item<2-> good agreement with experiments, however\ldots
                \item<3-> \ldots ``carbon mystery'' remains
                \item<4-> studied effects included:
                \begin{itemize}
                    \item<4-> \emph{trace approxmiation} -- small effect
                    \item<5-> \emph{collisions} -- small effect 
                    \item<6-> \emph{magnetic shear} -- small effect, except for He
                \end{itemize}
                \item<7-> good agreement between models:
                \begin{itemize}
                    \item<7-> $PF$ well below \emph{neo-classical}
                \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{6cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{fig_PF_Z_log}\\
                $PF$ scaling with impurity charge
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Comparing models}
%    \TODO{FIX THIS}
%    \begin{columns}[c]
%        \begin{column}{6cm}
%            \begin{center}
%                {\footnotesize
%                Trapped~Electron~mode: \\
%                \includegraphics[width=0.99\textwidth]{Z_TEM}
%                }%
%            \end{center}
%        \end{column}
%        \begin{column}{6cm}
%            \begin{center}
%                {\footnotesize
%                Ion~Temperature~Gradient~mode: \\ 
%                \includegraphics[width=0.99\textwidth]{fig_PF_Z_log}
%                }%
%            \end{center}
%        \end{column}
%    \end{columns}
%\end{frame}

\subsection{Paper II}

\begin{frame}
    \frametitle{\small Impurity transport in temperature gradient driven turbulence \mycite{Skyman, Nordman, Strand; \emph{Physics of Plasmas} (submitted)}}
    Enter: the \emph{trapped electron} mode

    \begin{columns}[c]
        \begin{column}{6cm}
            \begin{itemize}
                \item<2->{Primary focus: \emph{model} comparission:}
                \begin{itemize}
                    \item<2-> scalings with $Z$ \uncover<5>{(TE~\emph{and}~ITG)}
                    \item<3-4> scalings with driving gradients
                    \item<4-4> peaking of \emph{background}
                \end{itemize}
                \item<2->{Models show good agreement}
                \item<5->{Secondary focus: comparing \emph{modes}}
            \end{itemize}
        \end{column}
        \begin{column}{6cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{Z_TEM} \\
                $Z$-scaling for the \emph{TE}~mode
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{ITG  vs. TE mode scalings}
    Scalings of $PF$ with impurity charge $Z$
    \begin{itemize}
        \item<2-> Difference due to \emph{sign} of contribution to \emph{pinch}:
    \end{itemize}
    \begin{columns}[c]
        \begin{column}{6cm}
            \begin{center}
                {\footnotesize
                Trapped~Electron~mode: \\ 
                \includegraphics[width=0.99\textwidth]{Z_TEM} \\
                \uncover<2->{$V_{\grad T} \sim \frac{1}{Z} < 0$}
                }%
            \end{center}
        \end{column}
        \begin{column}{6cm}
            \begin{center}
                {\footnotesize
                Ion~Temperature~Gradient~mode: \\ 
                \includegraphics[width=0.99\textwidth]{fig_PF_Z_log} \\
                \uncover<2->{$V_{\grad T} \sim \frac{1}{Z}> 0$}
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Paper III}

\begin{frame}
    \frametitle{\small Particle transport in density gradient driven TE mode turbulence \mycite{Skyman, Nordman, Strand; \emph{Nuclear Fusion} (submitted)}}
    Density gradient driven TE mode turbulence 

    \begin{columns}[c]
        \begin{column}{6cm}
            \begin{itemize}
                \item<2->Complements Paper II:
                \begin{itemize}
                    \item<2-> steep density gradients --
                    e.g. \emph{transport barriers}
                \end{itemize}
                \item<3-> models agree well
                \item<4-> no background pinch ($PF_p=0$)
                \item<5-> impurity peaking $\ll$ driving gradient
            \end{itemize}
        \end{column}
        \begin{column}{6cm}
            \begin{center}
                {\footnotesize
                \includegraphics[width=0.99\textwidth]{omn_TEM} \\
                Scaling of $PF$ with \emph{density gradient}
                }%
            \end{center}
        \end{column}
    \end{columns}
\end{frame}


\section*{Summary}
\subsection*{Summary}
\begin{frame}
    \frametitle{Summary}
    \begin{itemize}
        \item<1-> Summary and conclusions:
        \begin{itemize}
            \item<2-> turbulent transport is important for fusion
            \item<3-> impurity simulations agree with experiments
            \item<4-> fluid and kinetic models show reasonable agreement
        \end{itemize}
    \end{itemize}
    \begin{itemize}
        \item<5-> Future avenues:
        \begin{itemize}
            \item<5-> effects of
            \begin{itemize}
                \item<5-> rotation
                \item<5-> realistic geometry
            \end{itemize}
            \item<6-> main ion transport
            \item<7-> transport barriers
        \end{itemize}
    \end{itemize}
    \begin{center}
        \visible<7->{\Large \bf Thank you!}
    \end{center}
\end{frame}

\end{document}


