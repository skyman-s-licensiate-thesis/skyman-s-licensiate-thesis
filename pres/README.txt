Hej,

Här kommer en hög med filer.

De två första är .sty-filer. Jag har en variant tänkt för kortare
presentationer. Och en för längre, med en ram som visar var i
presentation du är (då måste du använda \sections).

Sen kommer två gamla exempelfiler. Har inte tittat på dom på år. Så inte
säkert att allt fungerar till 100%. Dom är uppsatta för att fungera med
ett litet skript beamer, för att enkelt också producera "handouts". Jag
gör detta nu manuellt. Men är kanske av intresse.

Sen kommer en av mina presentationer. Som ett praktiskt exempel. Har
nyss upptäckt textpos. Rekommenderas.

Till sist, Chalmers-loggan som används. Det ska gå att använda latex och
pdflatex utan problem.

Bara fråga!

/Patrick



